from dataclasses import dataclass

import numpy as np


@dataclass
class Point3D:
    """
    Represents a point in 3D space
    """

    x: float
    """x Coordinate"""
    y: float
    """y Coordinate"""
    z: float
    """z Coordinate"""

    @property
    def as_homogeneous(self) -> np.ndarray:
        return np.array([self.x, self.y, self.z, 1])


@dataclass
class RelativePose3D:
    """
    Represents the relative pose or odometry in 3D space.
    """

    translation_x: float
    """Translation in the x-axis"""
    translation_y: float
    """Translation in the y-axis"""
    translation_z: float
    """Translation in the z-axis"""
    roll: float
    """Rotation around the x-axis (in radians)"""
    pitch: float
    """Rotation around the y-axis (in radians)"""
    yaw: float
    """Rotation around the z-axis (in radians)"""

    def __add__(self, other):
        """
        Operator overloading for addition to combine two relative poses.
        """
        return RelativePose3D(
            self.translation_x + other.translation_x,
            self.translation_y + other.translation_y,
            self.translation_z + other.translation_z,
            self.roll + other.roll,
            self.pitch + other.pitch,
            self.yaw + other.yaw,
        )

    def __str__(self):
        return f"""
            RelativePose3D(
                translation_x = {self.translation_x},
                translation_y = {self.translation_y},
                translation_z = {self.translation_z}, 
                roll          = {self.roll},
                pitch         = {self.pitch},
                yaw           = {self.yaw}
            )
        """

    @property
    def translation(self) -> np.ndarray:
        return np.array([self.translation_x, self.translation_y, self.translation_z])

    @property
    def rotation_matrix(self) -> np.ndarray:
        """
        Compute the 3x3 rotation matrix from roll, pitch, and yaw angles.
        """
        # Assuming the rotation order is roll-pitch-yaw (XYZ Euler angles).
        R_x = np.array(
            [
                [1, 0, 0],
                [0, np.cos(self.roll), -np.sin(self.roll)],
                [0, np.sin(self.roll), np.cos(self.roll)],
            ]
        )
        R_y = np.array(
            [
                [np.cos(self.pitch), 0, np.sin(self.pitch)],
                [0, 1, 0],
                [-np.sin(self.pitch), 0, np.cos(self.pitch)],
            ]
        )
        R_z = np.array(
            [
                [np.cos(self.yaw), -np.sin(self.yaw), 0],
                [np.sin(self.yaw), np.cos(self.yaw), 0],
                [0, 0, 1],
            ]
        )
        return np.dot(R_z, np.dot(R_y, R_x))

    @property
    def transformation_matrix(self):
        """
        Compute the 4x4 transformation matrix for this relative pose.
        """
        matrix = np.identity(4)
        matrix[:3, :3] = self.rotation_matrix
        matrix[:3, 3] = self.translation
        return matrix

    def apply_to_point(self, point: Point3D):
        """
        Apply the relative pose to a 3D point using a transformation matrix.
        """
        transformed_point = np.dot(self.transformation_matrix, point.as_homogeneous)
        return Point3D(transformed_point[0], transformed_point[1], transformed_point[2])


def create_trajectory(starting_point, relative_poses):
    """
    Create a trajectory in 3D space by successively applying relative poses to a starting point.
    """
    trajectory = [starting_point]
    current_point = starting_point
    for relative_pose in relative_poses:
        current_point = relative_pose.apply_to_point(current_point)
        trajectory.append(current_point)
    return trajectory


if __name__ == "__main__":
    starting_point = Point3D(0, 0, 0)
    # relative_poses = [
    #     RelativePose3D(1.0, 0.0, 0.0, np.pi/4, 0.0, 0.0),
    #     RelativePose3D(0.0, 1.0, 0.0, np.pi/4, np.pi/4, 0.0),
    #     RelativePose3D(0.0, 0.0, 1.0, np.pi/4, np.pi/4, np.pi/4),
    # ]

    relative_poses = [
        RelativePose3D(1.0, 0.0, 0.0, 0.0, 0.0, 0.0),
        RelativePose3D(1.0, 0.0, 0.0, 0.0, 0.0, 0.0),
        RelativePose3D(1.0, 0.0, 0.0, 0.0, np.pi, 0.0),
        RelativePose3D(1.0, 0.0, 0.0, 0.0, 0.0, 0.0),
        RelativePose3D(1.0, 0.0, 0.0, 0.0, 0.0, 0.0),
        RelativePose3D(1.0, 0.0, 0.0, 0.0, 0.0, 0.0),
    ]

    trajectory = create_trajectory(starting_point, relative_poses)

    for i, point in enumerate(trajectory):
        print(f"Point {i}: x={point.x}, y={point.y}, z={point.z}")
